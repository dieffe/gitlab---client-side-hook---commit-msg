# GitLab
## Client Side Hook

### commit-msg

The commit-msg hook takes one parameter, which again is the path to a temporary file that contains the commit message written by the developer. If this script exits non-zero, Git aborts the commit process, so you can use it to validate your project state or commit message before allowing a commit to go through. In the last section of this chapter, I’ll demonstrate using this hook to check that your commit message is conformant to a required pattern.

### Configuration

Copy/create commit-msg in path <local-project>\.git\hooks

### Check commit messages for JIRA issue keys

```python
#!/usr/bin/env python
import sys, re

#Required parts 
requiredRegex = "[a-z]+[0-9]*[a-z]*\-[0-9]+"

#Get the commit file
commitMessageFile = open(sys.argv[1]) 
commitMessage = commitMessageFile.read().strip()
  
if re.search(requiredRegex, commitMessage, re.IGNORECASE) is None:
    print "[POLICY] The commit doesn't reference a JIRA issue"
    sys.exit(1)

print "Commit message is validated"
sys.exit(0)
```
